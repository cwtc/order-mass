\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

GloriaPatri = \relative c' {
	\clef treble
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\mark \markup{ \fontsize #-2 \italic {Gloria Patri}}
	\ph
	g'4^\markup{ \italic {bow head}}( fis) g4( a4) a1 \bar "|"
	a4 c4 b4 a4 a4 b2 \bar "||"
	\ph
	a4^\markup{ \italic {raise head}}( fis4) g4( a4) a1 \bar "|"
	c4 b4 a2 b2 \bar "|"
	a4 a4 fis4 g4 e2 d2^\markup { \italic {All repeat antiphon} }  \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar Glo -- ry "be to the Father, and to the Son,"
		and to the Ho -- ly Ghost:
	\bold
	\Rbar As __ it __ "was in the beginning, is now, and"
		e -- ver shall be,
		world with -- out end. A -- men.
}

\score {
	\new Staff <<
	\new Voice = "One" {\GloriaPatri}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

