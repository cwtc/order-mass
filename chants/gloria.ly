\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Introit = \relative c' {
	\clef treble
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	c4 d4 f4 f4 e4 f4 g2 \bar "||"
	\ph
	e4 f4 e4 d4 d4 g4 g4 a2 \bar "|"

	g4 a4 a2 b4 c4 a2 \bar "|"
	a4^\markup{ \italic {bow head}} a4 a4 g2 e4^\markup{ \italic {raise head}} f4 f4 e2 \bar "|"

	c4 d4 e4 e4 e4 g4 g4 e4 f2 e2 \bar "|"
	c4 d4 e2 f4 f4 e2 \bar "|"
	g4 g4 a4 g4 a4 c4 g2 \bar "|"

	e4 f4 e4 g4 g4 f4 g4 a2 f4^\markup{ \italic {bow head}} f4 e2 \bar "|"
	c4^\markup{ \italic {raise head}} d4 f2 e4 f4 g2 g4 f4 g4 a2 g2 \bar "|"

	e4 f4 f4 f4 e4 d4 e4 c4 d4 e2 \bar "|"
	f4 d4 c4 d4 e2 \bar "|"

	g4 g4 a4 a4 a4 a4 g4 g4 f4 e4 f2 \bar "|"
	d4^\markup{ \italic {bow head}} c4 d4 e2 \bar "|"

	g4^\markup{ \italic {raise head}} g4 a4 a4 a4 a4 g4 a4 a4 c4 b4 a2 g2 \bar "|"
	e4 f4 e4 d4 e2 \bar "|"

	c4 d4 f4 e4 f4 g2 f2 \bar "|"
	g4 a4 g4 f4 e4 e2 \bar "|"

	g4 g4 a4 a4 c4 g4 e4 f4 f4 e2 \bar "|"
	c4 d4 f4 e4 f4 g4 f4 g4 a4 g4 f2 e2 \bar "|"
	d2 e2 \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar Glo -- ry be to God on high,
	\bold
	\Rbar And in earth peace, good -- will towards men.

		We praise thee, we bless thee,
		we wor -- ship thee, we glori -- fy thee.

		We give thanks to thee for thy great glo -- ry,
		O Lord God, heaven -- ly king,
		God the Fa -- ther Al -- might -- y.

		O Lord, the only be -- gott -- en Son, Je -- su Christ,
		O Lord God, Lamb of God, Son of the Fa -- ther,

		that tak -- est a -- way the sin of the world,
		have mer -- cy up -- "on us."

		Thou that tak -- est a -- way the sin of the world,
		re -- ceive our prayer.

		Thou that sitt -- est at the right hand of God the Fa -- ther,
		have mer -- cy up -- "on us."

		For thou on -- ly art ho -- ly,
		thou on -- ly art the Lord;

		thou on -- ly, O Christ, with the Ho -- ly Ghost,
		\markup { \concat { \cross art } } most high in the glo -- ry of God the Fa -- ther.
		A -- men.

}

\score {
	\new Staff <<
	\new Voice = "One" {\Introit}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

