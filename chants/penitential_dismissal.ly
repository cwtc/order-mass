\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Sursum = \relative c' {
	\clef treble
	\key f \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	a'4( bes4 a4 g4 a2) d,2
	f4( g4 a4 bes4 a4 g4 f4 e4 d4) c4( d4) e2 \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar Let __ us bless __ the __ Lord.
}

LyricsTwo = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\bold
	\Rbar Thanks __ { \skip 1 } be __ to __ God.
}

\score {
	\new Staff <<
	\new Voice = "One" {\Sursum}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	\new Lyrics \lyricsto "One" {\LyricsTwo}
	>>
}

