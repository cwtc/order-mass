\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Introit = \relative c' {
	\clef treble
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\override ParenthesesItem.font-size = #2
	\hide Staff.Stem
	\cadenzaOn
	\mark \markup{ \fontsize #-2 \italic {Antiphon}}
	\ph
	d4( g4) fis4 g4( a4) a1 \bar "|"
	a4( c4) b4 b4( a4) a2( b2) \bar "||" \noBreak
	\ph
	a4( fis4) g4( a4) a1 \bar "|" \noBreak
	a4( b4 c4) a4 g2 e2( d2) \bar "||" \break
	\ph \ph
	\startParenthesis \parenthesize
	g4( fis4) g4( \endParenthesis \parenthesize  a4 )
	a1 c4 b4 a2 b2 \bar "||"
	\ph
	a1 fis4 g4 e2 d2 \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar _ _ _ _
	_ _ _ _ _
	\bold
	\Rbar _ _ _ 
	_ _ _ _
	\normal
	\Vbar _ _ _ _ _ _ _ _
	\bold
	\Rbar _ _ _ _ _
}

\score {
	\new Staff <<
	\new Voice = "One" {\Introit}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

