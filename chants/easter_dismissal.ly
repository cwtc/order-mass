\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Sursum = \relative c' {
	\clef treble
	\key d \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	a'4 a4 a4 b2 a4 g4 a4( b4) b2
	b4 a4( d4 cis4 b4 a4) g4( a4 b4) b2( a2) \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar De -- part in peace, al -- le -- lu -- ia,
	al -- le -- lu -- ia. __
}

LyricsTwo = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\bold
	\Rbar Thanks be to God, al -- le -- lu -- ia,
	al -- le -- lu -- ia. __
}

\score {
	\new Staff <<
	\new Voice = "One" {\Sursum}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	\new Lyrics \lyricsto "One" {\LyricsTwo}
	>>
}

