\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Tract = \relative c' {
	\clef treble
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	f4 g4( a4) a1 \bar "|"
	a4( c4) a4 a4( g4) g2( a2) \bar "||"
	\ph
	g4( f4) g4( a4) a1 \bar "|"
	a4( c4) a4 g4 f4 f2 d2 \bar "|"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar _ _ _ _ _ _ _ _
	\bold
	\Rbar
}

\score {
	\new Staff <<
	\new Voice = "One" {\Tract}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

