\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Alleluia = \relative c' {
	\clef treble
	\key f \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\mark \markup{ \fontsize #-2 \italic {Alleluia}}
	\ph
	f4( g4 a4) f4( g4) g4( f4) f2 \bar "||"
	\ph
	f4( g4 a4) f4( g4) g4( f4) f2( g4 a4 c4 a2 g4 f4 g2 f2) \bar "||"
	\ph
	f4 g4 a1 g2 a2 \bar "||"
	\ph
	a1 f4 g4( a4) g4 f2^\markup{ \italic { All repeat alleluia \Rbar} } \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar Al -- le -- lu -- ia.
	\bold
	\Rbar Al -- le -- lu -- ia. __
	\normal
	\Vbar _ _ _ _ _ _ 
	\bold
	\Rbar
}

\score {
	\new Staff <<
	\new Voice = "One" {\Alleluia}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

