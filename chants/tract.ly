\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Tract = \relative c' {
	\clef treble
	\key aes \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	ees4 f4 aes1 bes2 aes2 \bar "||"
	\ph
	aes1 f4 ees2 \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar _ _ _ _ _
	\bold
	\Rbar
}

\score {
	\new Staff <<
	\new Voice = "One" {\Tract}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

