\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Sursum = \relative c' {
	\clef treble
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g'4( a4) b2 g4( a4) b4( a4) a2 \bar "||" \break
	\ph
	a4( b4 c4) b4 a4( b4) a2( g2) \bar "||" \noBreak
	\ph
	b4 a4 c4 b4 a4 b4 a4 a2( g2) \bar "||" \break
	\ph
	b4 a4( g4) b4( a4 b4) c4( b4) a4( g4) g4( a4) b4 a4( b4) a2 \bar "||" %\noBreak
	\ph
	b4 b4 a4 c4 b4 a4( b4) a4 a2( g2) \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar The __ Lord be __ with __ you. __
	\Vbar Lift __ up your __ hearts. __
	\bold
	\Rbar We lift them up un -- to the Lord. __
	\normal
	\Vbar Let us __ give __ thanks __ un -- to __ our Lord __ God.
	\bold
	\Rbar It is meet and right so __ to do. __
}

LyricsTwo = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\bold
	\Rbar And __ with thy __ Spi -- rit. __
}

\score {
	\new Staff <<
	\new Voice = "One" {\Sursum}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	\new Lyrics \lyricsto "One" {\LyricsTwo}
	>>
}

