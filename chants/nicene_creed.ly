\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Introit = \relative c' {
	\clef treble
	\key f \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	d4 d4 f4 f4 g2 a2 \bar "||"
	\ph
	a4 a4 a4 bes4 c4 a4 \bar "|"
	g4 f4 g4 a4 a4 f4 e2 \bar "|"
	d4 e4 f4 g4 g4 e4 f4 e4 d2 \bar "|"

	f4 f4 g4 a4 bes4 g4 a2 \bar "|"
	a4 a4 a4 bes4 c4 a4 d4 d4 c2 \bar "|"
	a4 a4 a4 f4 g4 a4 d,4 e4 f4 d4 d2 \bar "|"

	a'4 g4 a2 a4 g4 a2 \bar "|"
	f4 g4 a4 c4 bes4 bes4 a2 \bar "|" 
	f4 g4 g4 e4 f2 \bar "|"
	d4 d4 e4 f4 g4 d4 g4 f4 e2 d2 \bar "|"
	a'4 g4 f4 c'4 bes4 a2 \bar "|"

	f4 g4 a4 f2 \bar "|"
	d4 f4 g4 e4 f4 d2 \bar "|"
	a'4 a4 g4 a2 \bar "|"
	d,4^\markup{ \italic genuflect} e4 f4 g4 g4 g4 g4 a4 bes4 c2 \bar "|"
	a4 a4 g4 f4 bes2 a2 \bar "|"
	f4 g4 a2 f2 \bar "|"

	d4^\markup{ \italic stand} f4 g4 g4 e4 g4 f4 d4 d2 \bar "|"
	%%%
	g4 a4 a4 g4 a4 f2 \bar "|"
	a4 a4 a4 a4 g4 a4 a4 d,2 \bar "|"

	d4 e4 f4 g4 g4 c4 bes4 a4 
	g4 f4 d4 f4 e4 d2 \bar "|"
	a'4 bes4 c4 a4 bes4 c4 d2 \bar "|"
	c4 a4 a4 a4 g4 a4 f4 g4 a4 f2 e2 \bar "|"

	a4 a4 a4 f4 g4 a4 g4 c4 a2 \bar "|"
	a4 a4 g4 e4 f4 e4 d4 d2 \bar "|"
	f4 g4 g4 e4 f4 e4 d2 \bar "|"

	a'4 a4 g4 a4 f4 g4 a4 a4 a4 \bar "|"
	g4 a2 bes4 c4 c4 a4 a2 \bar "|"
	d,4 e4 f4 g4 a4 bes4 c4 a4 d4 d4 c2 \bar "|"
	a4 a4 a4 c4 bes4 a4 g4 a4 bes4 a2 \bar "|"
	g4 a4 a4 f4 g4 e4 d4 d2 \bar "|"
	f4 g4 a4 bes4 g2 f2 \bar "|"

	a4 d4 d4 c4 a4 g4 a4 bes4 g4 g4
	f4 g4 a4 a4 g4 a2 \bar "|"

	f4 f4 g4 f4 g4 a4 a4 \bar "|"
	d,4 f4 e4 f4 d4 e4 d2 \bar "|"

	f4 g4 a4 g4 g4 a4 bes4 c4 a4 g4 g4 f2 \bar "|"
	a4 a4 c4 a4 g4 f4 e4 d2 \bar "|"
	c2 d2 \bar "||"

	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar I be -- lieve in one God,
	\bold
	\Rbar the Fa -- ther Al -- might -- y,
		ma -- ker of hea -- ven and earth,
		and of all things visi -- ble and in -- visible;

		and in one Lord Je -- sus Christ,
		the on -- ly be -- gott -- en Son of God,
		be -- gott -- en of his Fa -- ther be -- fore all worlds;

		God of God, Light of Light,
		Ve -- ry God of Ve -- ry God,
		be -- gott -- en, not made,
		be -- ing of one sub -- stance with the Fa -- ther,
		through whom all things were made;

		who for us men,
		and for our sal -- va -- tion,
		came down from heaven,
		and was in -- car -- nate by the Ho -- ly Ghost
		of the Vir -- gin Ma -- ry,
		and was made man;

		and was cru -- ci -- fied al -- so for us
		%%%
		un -- der Pon -- tius Pi -- late.
		He suf -- fered and was bu -- ri -- ed.

		And the third day he rose a -- gain
		ac -- cord -- ing to the Scriptures,
		and as -- cend -- ed in -- to heaven,
		and sit -- teth on the right hand of the Fa -- ther.

		And he shall come a -- gain with glo -- ry
		to judge both the quick and the dead,
		whose king -- dom shall have no end.

		And I be -- lieve in the Ho -- ly Ghost,
		the Lord and gi -- ver of life,
		who pro -- ceed -- eth from the Fa -- ther and the Son,
		who with the Fa -- ther and the Son to -- gether
		is wor -- ship -- ped and glo -- ri -- fied,
		who spake by the pro -- phets.

		And I be -- lieve one ho -- ly, ca -- tho -- lic,
		and a -- po -- sto -- lic church.

		I ac -- know -- ledge one Bap -- tism
		for the re -- miss -- ion of sins.

		And I look for the re -- sur -- rec -- tion of the dead,
		and the \markup { \concat { \cross life } }
			of the world to come. A -- men.
}

\score {
	\new Staff <<
	\new Voice = "One" {\Introit}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

