\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Introit = \relative c' {
	\clef treble
	\key g \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\override ParenthesesItem.font-size = #2
	\hide Staff.Stem
	\cadenzaOn
	\mark \markup{ \fontsize #-2 { \italic Antiphon }}
	\ph
	f4 f4( g4) g4( ees4 g4 f4) f4( g4 f4) f2 \bar "|"
	f4 f4( g4 bes4 a4) a4( g4) bes2 g4( a4) f4( g4) \bar "|"
	\endOfLine \mark \markup{ \fontsize #-2 { \caps Ezekiel 47}}
	f4( bes2 c4 g4) g4( f4 g4 a4 g2) \bar "|"
	f4 f4( g4) f4( bes2 c4) g4( f4) g4( bes4 f4 g4) f2( ees2) \bar "|"
	%%%%
	f4 f4( g4 ees4 f4) f4( g4 bes4 g4 a4) g2( f2) \bar "||"

	\ph
	f4( g4) f2( g4 a4 bes4) bes4 a4( g4) a4( bes4) a4 \bar "|"
	bes2( a4) g4( c4) bes4( a4 bes4 c4 bes4 a4 bes4 a4 g4 f4 g2 f2) \bar "|"
	f4( bes4) bes2( c4) bes4 bes2( c4 d4 c4) bes4( a4 g4) g2 \bar "|"
	g4( f4) g4( bes4 f4 g4) f2( ees2) \bar "|"
	f4( g4) g4 g4( a4 g4) g2 \bar "|"
	g4( f4 ees4) f4( g4 bes2 g2 a4 bes4) f4( g4 f4) f2 \bar "||"
	\break

	\mark \markup{ \fontsize #-2 { \italic Verse }}
	\ph
	f4 g4( f4) f4( bes4) bes2
	bes4( a4) bes4( c4) c4 bes4( c4) bes2 \bar "||"
	\ph
	bes4( g4) g4( bes4) bes2 bes2( a4) f4( g4) bes4( a4) g2 f2 \bar "||"

	\mark \markup{ \fontsize #-2 { \italic {Gloria Patri} }}
	\ph
	f4 g4( f4) f4( bes4) bes2 \bar "|"
	bes4( a4) bes4( c4) c4 bes4( c4) bes4 bes2 \bar "||"
	\ph
	bes4( g4) g4( bes4) bes1 bes4( a4) bes4( c4) c4 bes4( c4) bes2 \bar "|"
	bes4 bes2( a4) f4( g4) bes4( a4) g2 f2 \bar "||"


	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar I be -- held __ wa -- ter
	which pro -- ceed -- ed from __ the __
	tem -- ple __
	on the __ right __ side __ there -- of, __
	%%%%
	al -- le -- lu -- ia: __

	\bold
	\Rbar and __ all __ they to __ whom __ that
	wa -- ter __ came __
	were __ heal -- ed ev -- ery __ one,
	and __ they __ say, __
	al -- le -- lu -- ia,
	al -- le -- lu -- ia.

	\normal
	\Vbar O give __ thanks __ "unto the Lord,"
	for __ he __ is gra -- cious:
	\bold
	\Rbar and __ his __ "mercy en" -- du -- reth __ for __ ev -- er. __

	\normal
	\Vbar Glo -- ry __ be __ "to the Father, and to the Son,"
	and __ to __ the Ho -- ly Ghost:
	\bold
	\Rbar As __ it __ "was in the beginning, is now," and __ ev -- er shall __ be:
	world with -- out __ end. __ A -- men.
}

\score {
	\new Staff <<
	\new Voice = "One" {\Introit}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

