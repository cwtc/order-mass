\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

OurFather = \relative c' {
	\clef treble
	\key f \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	f4( g4) a2 \bar "||"
	\ph
	g4 bes4 a4 g2 f2 \bar "|"
	a4 g4( f4) f4( g4) a4 g4( a4) g2 \bar "|"
	a4 bes4 a4 g2( f2) \bar "|"
	g4 a4 g4 g2( f2) \bar "|"
	g4 a4 g4( f4) f4 f4( g4) a4 g4( a4) g2 \bar "|"
	f4 g4 a4 bes4 a4 g4 a4 g2( f2) \bar "|"
	g4 a4 bes4 a4 g4 g4 f2 \bar "|"
	g4 g4 f4 g4 a4 g4 a4 g4 f4 g2 \bar "|"
	d4 f4 g4 a4 g4 f4 g4 g4( f4) f2 \bar "|"
	f4 f4 g4 g4 g4 a4 g4( f4) f2 \bar "|"
	d4 f4 g4 g4 a4 g2 \bar "|"
	f4 g4 g4 g4 a4 g4( f4) f2 \bar "|"
	f4 g4 g4 a4 g4( f4) f2 \bar "|"
	f4 f2( g2) \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar Our Father,
	\bold
	\Rbar who art in hea -- ven,
		hal -- low -- ed __ be thy __ name;
		thy king -- dom come; __
		thy will be done, __
		on earth as __ it is __ in hea -- ven.
		Give us this day our dai -- ly bread, __
		and for -- give us our tres -- passes,
		as we for -- give them that tres -- pass a -- "gainst us."
		And lead us not in -- to temp -- ta -- tion,
		but de -- li -- ver us from e -- vil.
		For thine is the king -- dom,
		the pow -- er and the glo -- ry,
		for -- ev -- er and ev -- er.
		A -- men. __
}

\score {
	\new Staff <<
	\new Voice = "One" {\OurFather}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

