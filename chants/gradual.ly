\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Gradual = \relative c' {
	\clef treble
	\key a \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\override Staff.KeyCancellation.break-visibility = #all-invisible
	\cadenzaOn
	\ph
	d4 fis4 a1 b4 b4 a2 \bar "||"
	\ph
	fis4 a1 b4 g4 a2 fis4( e4 fis4 e4 d2) \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar _ _ _ _ _ _
	\bold
	\Rbar
}

\score {
	\new Staff <<
	\new Voice = "One" {\Gradual}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

