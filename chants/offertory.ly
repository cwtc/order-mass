\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Tract = \relative c' {
	\clef treble
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	d4 e4( d4) d4( g4) g1 \bar "|"
	g4 g4( fis4) g4( a4) a4 g4( a4) g2 \bar "||"
	\ph
	g4( e4) e4( g4) g1 \bar "|"
	a4 g4( e4) g4 fis4( d4) e4( fis4) e2 \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\normal
	\Vbar _ _ _ _ _ _ _ _ _ _
	\bold
	\Rbar
}

\score {
	\new Staff <<
	\new Voice = "One" {\Tract}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

