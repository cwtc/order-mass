# An order for Solemn High Mass

A liturgical booklet for an Anglo-Catholic style of Solemn High Mass. It reflects the usage of the Church of St. John the Evangelist in Montreal, QC, but is free for use/adaptation by anyone.

Includes:
* *Asperges Me*
* *Vidi Aquam*
* Order for Mass with notated chant
* Last Gospel
* *Angelus*
* *Regina Caeli*

## Setup

To compile this booklet, you will minimally need a TeX distribution and LilyPond for music engraving. If you are not sure what this means, or do not have these on your computer already, you can find installation scripts that aim to streamline the process in the project [releases](https://gitlab.com/cwtc/order-mass/-/releases) section.

For now, an install script is only included for Windows. MacOS and Linux users will have to install these parts themselves.

## Use

The main content of the booklet is found in `order_mass.lytex`. To compile it, open a command line window, navigate to the directory where you have stored `order-mass`, and run the command according to your operating system:

## Windows

```
scripts/compile.bat
```

MacOS and Linux users do not yet have a compile script. Support is coming.
