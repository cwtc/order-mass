# Tools
echo "Installing sumatrapdf..."
choco install -y sumatrapdf
echo "Installing Sublime Text 3..."
choco install -y sublimetext3
choco install -y sublimetext3.packagecontrol
echo "Installing Python 3..."
choco install -y python3

# Project files
echo "Installing git..."
choco install -y git
&cmd.exe /c rd /s /q order-mass
git clone "https://gitlab.com/cwtc/order-mass.git" "order-mass"

# Project software
echo "Installing JDK 8..."
choco install -y jdk8
echo "Installing lilypond..."
choco install -y lilypond
cmd /c mklink "C:\Program Files (x86)\LilyPond\usr\bin\lilypond.exe" "C:\tools\lilypond.exe"
cp lilypond-book.py "C:\Program Files (x86)/LilyPond/usr/bin"
echo "Installing tinytex..."
choco install -y tinytex
C:\tools\TinyTeX\bin\win32\tlmgr install "book-of-common-prayer"
