import argparse
import os
import shutil
import subprocess


def run_lilypond(file):
    lilypond_cmd = ['lilypond-book', '--pdf']
    lilypond_cmd.append('--output=out')
    lilypond_cmd.append(file)
    lilypond_proc = subprocess.Popen(lilypond_cmd, shell=True)
    lilypond_proc.communicate()


def run_xelatex(file_name):
    xelatex_cmd = ['xelatex', file_name]
    xelatex_proc = subprocess.Popen(xelatex_cmd, shell=True)
    xelatex_proc.communicate()


if __name__ == '__main__':
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument('lytex_file', type=str, help='Path to .lytex file to compile.')
    args = parser.parse_args()

    lytex_file_name = args.lytex_file.replace('.lytex', '')

    # Run lilypond-book
    run_lilypond(args.lytex_file)

    # Get list of generated files
    out_files = [os.path.join(path, name) for path, subdirs, files in os.walk('out') for name in files]
    # Get list of subdirs
    out_subdirs = list(set([x.split(os.sep)[1] for x in out_files]))
    print(out_subdirs)
    for subdir in out_subdirs:
        if len(subdir) == 2 or subdir.endswith('.tex'):  # .tex aren't subdirs, just on first level
            if os.path.exists(subdir):
                if os.path.isdir(subdir):
                    shutil.rmtree(subdir)
                else:
                    os.remove(subdir)
            shutil.move(os.path.join('out', subdir), subdir)
            # shutil.rmtree(subdir)

    # Compile xetex twice, to assure table of contents
    run_xelatex(lytex_file_name)
    run_xelatex(lytex_file_name)

    # Clean up
    for subdir in out_subdirs:
        if len(subdir) == 2:
            shutil.rmtree(subdir)
    shutil.rmtree('out')
    for ext in ['aux', 'dep', 'log', 'toc']:
        try:
            os.remove('{0}.{1}'.format(lytex_file_name, ext))
        except FileNotFoundError:
            pass
